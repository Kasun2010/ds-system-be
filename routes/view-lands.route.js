const express = require('express');
const lands = require('../controllers/view-land-info.controller');
const router = express.Router();
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

router.get('/',lands.viewLandInfo);

module.exports = router;