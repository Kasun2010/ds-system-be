const express = require('express');
const persons = require('../controllers/view-personal-info.controller');
const buildings = require('../controllers/view-building-info.controller');
const router = express.Router();
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

router.get('/',persons.viewPersonalInfo);
router.get('/',buildings.viewBuildingInfo);


module.exports = router;