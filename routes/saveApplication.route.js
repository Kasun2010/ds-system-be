const express = require('express');
const save = require('../controllers/application-save.controller');
const building=require('../controllers/save-building-application.controllerr');
const land=require('../controllers/save-land-application.controller');
const router = express.Router();
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

router.route('/')
    .postMessage(save.savePersonalInfoApplication());

router.route('/')
    .postMessage(building.saveBuildingApplication());

router.route('/')
    .postMessage(land.saveLandApplication());

module.exports = router;