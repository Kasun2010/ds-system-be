const express = require('express');
const persons = require('../controllers/view-by-id.controller');
const router = express.Router();
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

router.get('/api/persons/:id',persons.viewPersonalInfoById);

module.exports = router;