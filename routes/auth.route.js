const express = require('express');
const auth = require('../controllers/auth.controller');
const router = express.Router();
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

router.route('/')
    .postMessage(auth.authenticate());

module.exports = router;