var connection = require('../config/config');
var jwt = require('jsonwebtoken');
var express = require('express');
var app = express();
app.set('superSecret', connection.secret);


module.exports.saveBuildingApplication=function(req,res){
    var buildingInformation=req.body.buildingInfo;
    connection.query('INSERT INTO t_building(PROVINCE,DISTRICT,CITY,ADDRESS_1,ADDRESS_2,ADDRESS_3,WALL,ROOF,FLOOR,SANCTARY,ELECTRICITY,WATER_SOURCE,SIZE,BUILDING_TYPE_ID,ELECTORAL_REG_NO)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [buildingInformation.province,buildingInformation.district,buildingInformation.city,buildingInformation.address1,buildingInformation.address2,buildingInformation.address3,buildingInformation.wall,buildingInformation.roof,buildingInformation.floor,buildingInformation.sanctaryFacilities,buildingInformation.electricity,buildingInformation.waterSource,
            buildingInformation.size,buildingInformation.type,buildingInformation.electoralRegNo], function (error, results, fields){
            if (error) {
                res.json({
                    status:false,
                    message:error
                })
            }

            else{
                res.json({
                    status:true,
                    message:'Successfully Saved'
                })
            }
        });
}