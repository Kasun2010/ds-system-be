var connection = require('../config/config');
var jwt = require('jsonwebtoken');
var express = require('express');
var app = express();
app.set('superSecret', connection.secret);


module.exports.saveLandApplication = function (req,res) {
    var landInformation=req.body.landInformation;
    connection.query('INSERT INTO t_land(PROVINCE,DISTRICT,CITY,ADDRESS_1,ADDRESS_2,ADDRESS_3,LAND_TYPE,SIZE,CULTIVATION,LICENSE_NO,LICENSE_TYPE,OWNER_NAME,OWNER_ID,REG_NO,PLAN_NO,LOT_NO)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [landInformation.province,landInformation.district,landInformation.city,landInformation.address1,landInformation.address2,landInformation.address3,landInformation.landType,landInformation.size,landInformation.cultivations,landInformation.licenseNo,landInformation.licenseType,landInformation.ownerName,
            landInformation.ownerShip,landInformation.regNo,landInformation.planNo,landInformation.lotNo], function (error, results, fields){
            if (error) {
                res.json({
                    status:false,
                    message:error
                })
            }
            else{
                res.json({
                    status:true,
                    message:'Successfully Saved'
                })
            }
        });
}