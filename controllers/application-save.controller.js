var connection = require('../config/config');
var jwt = require('jsonwebtoken');
var express = require('express');
var app = express();
app.set('superSecret', connection.secret);

/**
 * Execute all queries at once to save the application
 * @author Kasun
 * @param req
 * @param res
 */
module.exports.savePersonalInfoApplication=function(req,res){
    var personalInformation=req.body.personalInformation;
    connection.query('INSERT INTO t_member(PROVINCE,DISTRICT,CITY,ADDRESS_1,ADDRESS_2,ADDRESS_3,MEMBER_FULL_NAME,MEMBER_NIC,MEMBER_DOB,MEMBER_CIVIL_STATUS,MEMBER_GENDER,MEMBER_OCCUPATION,MEMBER_RELIGION,MEMBER_MOBILE,MEMBER_DRIVING_LICENSE,' +
        'MEMBER_SIZE,MEMBER_BIRTH_PLACE,MEMBER_CHARACTOR,MEMBER_DISABILITY,MEMBER_NATIONALITY,MEMBER_PASSPORT_NO)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [personalInformation.province,personalInformation.district,personalInformation.city,personalInformation.address1,personalInformation.address2,personalInformation.address3,personalInformation.name,personalInformation.nic,personalInformation.dob,personalInformation.civilStatus,personalInformation.gender,personalInformation.occupation,
            personalInformation.religion,personalInformation.mobileNo,personalInformation.drivingLicenseNo,personalInformation.size,personalInformation.birthPlace,personalInformation.charactor,personalInformation.disability,personalInformation.nationality,personalInformation.passportNo], function (error, results, fields){
            if (error) {
                res.json({
                    status:error.status,
                    message:error
                })
            }
            else{
                res.json({
                    status:true,
                    message:'Successfully Saved'
                })
            }
        });

}

