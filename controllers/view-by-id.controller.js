var connection = require('../config/config');
var jwt = require('jsonwebtoken');
var express = require('express');
var app = express();
app.set('superSecret', connection.secret);

/**
 * Execute all queries at once to save the application
 * @author Kasun
 * @param req
 * @param res
 */
module.exports.viewPersonalInfoById=function(req,res){
    personId=req.params.id;
    connection.query('SELECT MEMBER_ID AS id,PROVINCE AS province,DISTRICT AS district,CITY AS city,MEMBER_FULL_NAME AS name,MEMBER_NIC AS nic FROM t_member WHERE MEMBER_ID=?',[personId], function (error, results, fields){
        if (error) {
            res.json({
                status:error.status,
                message:error
            })
        }
        else{
            res.json({

                data:results
            })
        }
    });



}



