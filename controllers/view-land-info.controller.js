var connection = require('../config/config');
var jwt = require('jsonwebtoken');
var express = require('express');
var app = express();
app.set('superSecret', connection.secret);

/**
 * Execute all queries at once to save the application
 * @author Kasun
 * @param req
 * @param res
 */
module.exports.viewLandInfo=function(req,res){

    connection.query('SELECT PROVINCE AS province,DISTRICT AS district,CITY AS city,LAND_TYPE as type, OWNER_NAME AS owner FROM t_land', function (error, results, fields){
            if (error) {
                res.json({
                    status:error.status,
                    message:error
                })
            }
            else{
                res.json({
                    status:false,
                    data:results
                })
            }

        });


}



