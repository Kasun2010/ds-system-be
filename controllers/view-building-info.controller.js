var connection = require('../config/config');
var jwt = require('jsonwebtoken');
var express = require('express');
var app = express();
app.set('superSecret', connection.secret);

/**
 * Execute all queries at once to save the application
 * @author Kasun
 * @param req
 * @param res
 */
module.exports.viewBuildingInfo=function(req,res){
    connection.query('SELECT PROVINCE AS province,DISTRICT AS district,CITY AS city,SIZE AS size,ELECTORAL_REG_NO AS elect_reg_no FROM t_building', function (error, results, fields){
        if (error) {
            res.json({
                status:error.status,
                message:error
            })
        }
        else{
            res.json({

                data:results
            })
        }
    });



}



